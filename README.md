# Arch Linux Setup

This is where I will provide documentation on my preferred setup of Arch. Of which some points may or may not be best practices or even correct :D.

## Pramble

A lot of this guide was "stolen" and modified from existing guides and wiki pages on the [Arch Wiki](https://wiki.archlinux.org/)

>Proceed with caution as I'm an Arch Newbie still!

## Let's roll

Depending on the architecture you are using, please see one of the following:

- [RaspberryPi 1, 2, or 3 (ARM)](docs/raspberry_pi.md)
- [64-bit Intel or AMD (x86_64)](docs/i586.md)
